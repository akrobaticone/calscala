# Michele Villa 2021-2023


import wx

#crea il pannello con i widget TODO riscrivere con i SIZER
class MyPanel(wx.Panel):
    def __init__(self, *a, **k):
        wx.Panel.__init__(self, *a, **k)
        segnali = ["0-20 mA", "4-20 mA", "0-1 V", "0-2 V", "0-5 V", "0-10 V"]
# Interfaccia grafica e oggetti nel panel
        self.combo = wx.ComboBox(self, choices=segnali, style=wx.CB_READONLY, pos=(35,110),size=(130,30))
        self.campo_basso= wx.TextCtrl(self, value="0", pos= (20,50), size=(80,30))
        self.lbl_basso=wx.StaticText(self, label="Inizio campo",pos=(20,30), size=(80,20), style=wx.ALIGN_CENTRE)
        self.campo_alto= wx.TextCtrl(self, value="0", pos= (120,50), size=(80,30))
        self.lbl_tipo=wx.StaticText(self, label="Tipo di segnale",pos=(35,90), size=(130,20), style=wx.ALIGN_CENTRE)        
        self.lbl_alto=wx.StaticText(self, label="Fine campo",pos=(120,30), size=(80,20), style=wx.ALIGN_CENTRE)
        self.In_segnale= wx.TextCtrl(self, value="0", pos= (20,190), size=(80,30),style=wx.TE_PROCESS_ENTER)
        self.lbl_In=wx.StaticText(self, label="Segnale",pos=(20,170), size=(80,20), style=wx.ALIGN_CENTRE)
        self.scala= wx.TextCtrl(self, value="0", pos= (120,190), size=(80,30),style=wx.TE_PROCESS_ENTER)
        self.lbl_scala=wx.StaticText(self, label="Valore",pos=(120,170), size=(80,20), style=wx.ALIGN_CENTRE)
        self.combo.SetValue("0-20 mA")
        self.linea1=wx.StaticLine(self, pos=(15,160), size=(200,2),style=wx.LI_HORIZONTAL)
        self.linea2=wx.StaticLine(self, pos=(109,160), size=(2,80),style=wx.LI_VERTICAL)

#definisci gli eventi
        self.In_segnale.Bind(wx.EVT_TEXT_ENTER,self.in_change)
        self.scala.Bind(wx.EVT_CHAR,self.tasto)
        self.In_segnale.Bind(wx.EVT_CHAR,self.tasto)
        self.campo_basso.Bind(wx.EVT_CHAR,self.tasto)
        self.campo_alto.Bind(wx.EVT_CHAR,self.tasto)
        self.scala.Bind(wx.EVT_TEXT_ENTER, self.scala_change)

# calcola la scala in funzione del segnale
    def in_change(self,evt):
        a= self.combo.GetValue()
        c_basso=float(self.campo_basso.GetValue())
        c_alto=float(self.campo_alto.GetValue())
        InSegnale=float(self.In_segnale.GetValue())

        if (c_alto-c_basso)==0:
            msg=wx.MessageBox("Campo alto e campo basso non possono coincidere!","Errore", style=wx.OK|wx.CENTRE)
            self.campo_alto.SetFocus()
        else:
            if a == "4-20 mA":            
                sca=((c_alto-c_basso)/16)*(InSegnale-4)+c_basso
            if a == "0-20 mA":            
                sca=((c_alto-c_basso)/20)*(InSegnale)+c_basso
            if a == "0-10 V":            
                sca=((c_alto-c_basso)/10)*(InSegnale)+c_basso
            if a == "0-5 V":            
                sca=((c_alto-c_basso)/5)*(InSegnale)+c_basso
            if a == "0-2 V":            
                sca=((c_alto-c_basso)/2)*(InSegnale)+c_basso
            if a == "0-1 V":            
                sca=(c_alto-c_basso)*(InSegnale)+c_basso

            self.scala.SetValue(str(round(sca,2)))
        evt.Skip()

#calcola il segnale in funzione della scala
    def scala_change(self,evt):
        a= self.combo.GetValue()
        c_basso=float(self.campo_basso.GetValue())
        c_alto=float(self.campo_alto.GetValue())
        Sca=float(self.scala.GetValue())
        Segnale=0
        if (c_alto-c_basso)==0:
            msg=wx.MessageBox("Campo alto e campo basso non possono coincidere!","Errore", style=wx.OK|wx.CENTRE)
            self.campo_alto.SetFocus()
        else:
            if a == "4-20 mA":            
                Segnale=(16/(c_alto-c_basso))*(Sca-c_basso)+4
            if a == "0-20 mA":            
                Segnale=(20/(c_alto-c_basso))*(Sca-c_basso)
            if a == "0-10 V":            
                Segnale=(10/(c_alto-c_basso))*(Sca-c_basso)
            if a == "0-5 V":            
                Segnale=(5/(c_alto-c_basso))*(Sca-c_basso)
            if a == "0-2 V":            
                Segnale=(2/(c_alto-c_basso))*(Sca-c_basso)      
            if a == "0-1 V":            
                Segnale=(1/(c_alto-c_basso))*(Sca-c_basso)      
            self.In_segnale.SetValue(str(round(Segnale,3)))
        evt.Skip()

#evita l'inserimento di caratteri non numerici
    def tasto(self,evt):
        keycode=evt.GetKeyCode()
        if (keycode>47 and keycode<58)or keycode==ord(".") or keycode==ord("-") or keycode==13 or keycode==8 or keycode==127:
            evt.Skip()
            return
        return
   		
#crea il frame nel quale viene visualizzato il pannello con i widget
class MyFrame(wx.Frame):
	def __init__(self, *a, **k):
		wx.Frame.__init__(self, style=wx.MINIMIZE_BOX | wx.CLOSE_BOX, size=(225, 270), title="Calscala", *a, **k)
		panel=MyPanel(self)

#avvia il loop principale

evento=False
app=wx.App()
frame=MyFrame(None)
frame.Show()
app.MainLoop()


